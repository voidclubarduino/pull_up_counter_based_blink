/**
 * Pull Up Counter Based Blink
 * Liga e desliga o LED em função do clique de um botão e do número de vezes que um outro botão, 
 * sendo que este último atua como contador, é pressionado.
 * 
 * 
 * Neste exemplo é utilizado o LED EXTERNO ligado ao pino 13, 
 * um botão com configuração PULL-UP ligado ao pino 4 (botão contador), 
 * e um botão com configuração PULL-UP ligado ao pino 5 (botão que despoleta a mudança de estado no LED).
 * 
 * O botão contador, cada vez que é pressionado, incrementa o número de vezes que o LED deverá ligar e desligar.
 * O botão responsável por despoletar a mundaça de estado no LED, 
 * liga e desliga o LED o mesmo número de vezes que se encontra registado no contador e, depois, coloca o contador a 0.
 * 
 * Devido a problemas a nível mecânico e físico, por vezes, os botões geram ruído, o qual, se não for tratado,
 * pode levar a que os programas assumam múltiplos cliques do botão quando o mesmo apenas foi clicado uma única vez.
 * O tratamento do ruído denomina-se debouncing. Este exemplo inclui a programação desse mesmo debouncing.
 *
 * criado a 28 de Abril 2016
 * por Bruno Horta, Ivo Oliveira
*/

#define BUTTON_COUNTER_PIN 4
#define BUTTON_LED_PIN 5
#define LED_PIN 13
/**
 * Variáveis necessárias para debouncing do Botão Contador
 */
int buttonStateCounter;             // the current reading from the input pin
int lastButtonStateCounter = LOW;   // the previous reading from the input pin
long lastDebounceTimeCounter = 0;

/**
 * Variáveis necessárias para debouncing do Botão responsável por despoletar o ligar e desligar do LED
 */
int buttonStateLED;
int lastButtonStateLED = LOW;
long lastDebounceTimeLED = 0;
/**
 * DEBOUNCE DELAY
 */
long debounceDelay = 50;
/**
 * COUNTER
 */
int counter = 0;

void setup() {
  Serial.begin(9600);
  /*
   * Definir o modo de funcionamento do botão contador
  */
  pinMode(BUTTON_COUNTER_PIN, INPUT);
  /*
   * Definir o modo de funcionamento do botão responsável por despoletar o ligar e desligar do LED
  */
  pinMode(BUTTON_LED_PIN, INPUT);
  /*
   * Definir o modo de funcionamento do LED
  */
  pinMode(13, OUTPUT);

}
void loop() {
  /**
   * Verificar se a leitura é válida para o botão contador ou se se trata de ruído
   */
  if(processDebounce(BUTTON_COUNTER_PIN, &lastDebounceTimeCounter, &buttonStateCounter, &lastButtonStateCounter)){
    /**
     * Visto a leitura ser válida,
     * Incrementar o Contador
     */
    incrementCounter();
  }
  
  /**
   * Verificar se a leitura é válida para o botão responsável por despoletar o ligar e desligar do LED ou se se trata de ruído
   */
  if(processDebounce(BUTTON_LED_PIN, &lastDebounceTimeLED, &buttonStateLED, &lastButtonStateLED)){
    transformCounterToLedBlink();
  }
}
/**
 * Liga e desliga o LED, em intervalos de 300 milisegundos, o mesmo número de vezes que se encontra registado no contador. 
 * Antes de terminar, coloca o contador a 0.
 */
void transformCounterToLedBlink(){
  for(int i = counter; i > 0 ; i--){
    digitalWrite(LED_PIN, HIGH);
    delay(300);
    digitalWrite(LED_PIN, LOW);  
    delay(300);
  }
  /**
   * reiniciar contador
   */
  counter=0;
}
/**
 * Incrementa o contador
 */
void incrementCounter(){
  counter++;
}
/**
 * Realiza uma leitura para um determinado pino e devolve true caso a leitura corresponda ao estado atual 
 * do botão associado ao pino e a leitura indique que o pino se encontra ativo (estado HIGH). 
 * Em qualquer outra situação a função retorna false, seja por o botão não se encontrar ativo, 
 * seja por a leitura ser considerada ruído (uma vez que o tempo decorrido desde a última leitura considerada válida 
 * não é superior ao intervalo de debouncing);
 */
boolean processDebounce(int pin, long *lastDebounceTime, int *buttonState, int *lastButtonState){
  // ler o estado do pino para uma variável local
  int sensorVal = digitalRead(pin);
  /*
   * verificar se o botão foi pressionado e o tempo que decorreu desde o último pressionar do botão 
   * é suficiente para ignorar qualquer tipo de ruído.
  */

  /*
   * Se a leitura registou uma alteração de estado, seja ele ruído ou o pressionar do botão
  */
  if (sensorVal != *lastButtonState) {
    // reiniciar o contador de debouncing
    *lastDebounceTime = millis();
  }
  if ((millis() - *lastDebounceTime) > debounceDelay) {
    /**
     * Qualquer que seja a leitura, esta aconteceu a um tempo superior ao intervalo de debouncing considerado (no exemplo 50 milisegundos).
     * Por essa razão, pode se assumir a leitura como sendo o estado atual do botão.
     */
    if (sensorVal != *buttonState) {
      /*
       * o estado atual do botão é diferente ao último estado válido registado, por isso,
       * igualar o último estado válido, para o botão, como sendo a leitura atual.
       */
      *buttonState = sensorVal;
      if (*buttonState == HIGH) {
        /*
         * definir o último estado lido, para o botão, como sendo a leitura atual.
         */
        *lastButtonState = sensorVal;
        /**
          * O botão encontra-se ativo, por isso retorna-se true
        */
        return true;
      }
    }
  }
  /*
   * definir último estado lido, para o botão, como sendo a leitura atual.
   */
  *lastButtonState = sensorVal;
  return false;
}




